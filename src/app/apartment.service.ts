import { Injectable } from '@angular/core';
import {Apartment} from "./apartment";

const APARTMENTS: Apartment[] = [
    {id: 1, moveInDate: '2017-01-01', city: 'Düsseldorf', postCode: '40477', street: 'Marc-Chagall-Str. 12',countryCode: 'DE', contactMailAddress: 'becker.dom@gmail.com'},
    {id: 2, moveInDate: '2012-12-01', city: 'Tönisvorst', postCode: '47918', street: 'Kemepenerstraße 1',countryCode: 'DE', contactMailAddress: 'dummy@test.de'},
    {id: 3, moveInDate: '2015-02-16', city: 'Düsseldorf', postCode: '42989', street: 'Adenauerweg 15',countryCode: 'DE', contactMailAddress: 'testmensch@test.com'},
    {id: 4, moveInDate: '1952-02-06', city: 'London', postCode: 'EC1A 1AA', street: 'Buckingham Palace 1',countryCode: 'GB', contactMailAddress: 'queen@monarch.org'}
];

@Injectable()
export class ApartmentService {

    /**
     * retrieves all apartments from the Database
     * @returns {Promise<Apartment[]>}
     */
    getAllApartments(): Promise<Apartment[]> {
        return new Promise(resolve => {
            setTimeout(() => resolve(APARTMENTS), 500);
        });
    }

    /**
     * gets a sepecific apartment by id from the database
     * @param id
     * @returns {Apartment}
     */
    getApartment(id): Apartment {
        return APARTMENTS[id];
    }

    /**
     * Saves a new apartment entry to the database
     * @param {Apartment} apartment
     * @returns {Object}
     */
    saveNewApartment(apartment: Apartment): object {
        return {success: true, message: 'Die neue Wohnung wurde gespeichert'};
    }

    /**
     * Updates an existing apartment given that the security token is valid
     * @param {Apartment} apartment
     * @param {string} securityToken
     * @returns {Object}
     */
    updateExistingApartment(apartment: Apartment, securityToken: string): object {
        return {success: true, message: 'Die neue Wohnung wurde gespeichert'};
    }
}