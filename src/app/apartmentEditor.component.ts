import { Component, Input, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { Location }                 from '@angular/common';
import {Apartment} from "./apartment";
import {ApartmentService} from "./apartment.service";
import {CountryService} from "./country.service";
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'apartment-editor',
    templateUrl: './apartmentEditor.component.html',
    styleUrls: ['./apartmentEditor.component.css'],
})
export class ApartmentEditorComponent implements OnInit {
    constructor(
        private apartmentService: ApartmentService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    @Input() apartment: Apartment;
    supportedCountries: String[][];
    apartmentId: number;
    securityToken: string;
    ngOnInit(): void {

        //try to fetch the params for id and securityToken
        this.route.params.subscribe(params => {
            this.apartmentId = +params['id'];
            this.securityToken = params['securityToken'];
        });

        //if there is an id, load the apartment from the database, otherwise create a new one
        if (this.apartmentId) {
            this.apartment = this.apartmentService.getApartment(this.apartmentId);
        } else {
            var today = new Date();
            var day = today.getDate() < 10 ? "0"+ today.getDate() : today.getDate();
            var month = (today.getMonth()+1) < 10 ? "0"+(today.getMonth()+1) : (today.getMonth()+1);
            var dateString = today.getFullYear()+'-'+month+'-'+day;
            this.apartment = new Apartment(0,dateString, "", "", "", "", "");
        }
        this.supportedCountries = CountryService.getSupportedCountries();
    }
    onSubmit(): void {
        if (this.apartment.id) {
            var result = this.apartmentService.updateExistingApartment(this.apartment,this.securityToken);
        } else {
            var result = this.apartmentService.saveNewApartment(this.apartment);
        }
        if (result.success) {
            alert(result.message);
            this.router.navigate(['apartments']);
        } else {
            alert("Es ist ein Fehler aufgetreten.\n" + result.message);
        }
    }
}
