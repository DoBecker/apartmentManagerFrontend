import { Component } from '@angular/core';
import {Apartment} from "./apartment";
import { OnInit } from "@angular/core"
import {ApartmentService} from "./apartment.service";
import {CountryService} from "./country.service";

@Component({
    selector: 'apartment-list',
    templateUrl: './apartmentList.component.html',
    styleUrls: ['./apartmentList.component.css'],
})

export class ApartmentListComponent implements OnInit{
    constructor(private apartmentService: ApartmentService) {}
    ngOnInit(): void {
        this.apartmentService.getAllApartments().then(apartments => this.apartments = apartments);
    }

    getCountryTextByCode(countryCode): string {
        return CountryService.getCountryNameByCode(countryCode);
    }

    apartments: Apartment[];
}
