/**
 * represantation of an Apartment
 */
export class Apartment {
    constructor(
    public id: number,
    public moveInDate: string,
    public street: string,
    public postCode: string,
    public city: string,
    public countryCode: string,
    public contactMailAddress: string
    ) {}
}
