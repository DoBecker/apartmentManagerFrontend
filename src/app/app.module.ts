import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router"

import {AppComponent} from './app.component';
import {ApartmentListComponent} from "./apartmentList.component";
import {ApartmentService} from "./apartment.service";
import {ApartmentEditorComponent} from "./apartmentEditor.component";
import {FormsModule} from "@angular/forms";
import {CountryService} from "./country.service";

@NgModule({
    declarations: [
        AppComponent,
        ApartmentListComponent,
        ApartmentEditorComponent
    ],
    imports: [
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'apartments',
                pathMatch: 'full'
            },
            {
                path: 'apartments',
                component: ApartmentListComponent
            },
            {
                path: 'apartmentEditor',
                component: ApartmentEditorComponent
            }
            ,
            {
                path: 'apartmentEditor/:id/:token',
                component: ApartmentEditorComponent
            }
        ]),
        BrowserModule,
        FormsModule
    ],
    providers: [ApartmentService,CountryService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
