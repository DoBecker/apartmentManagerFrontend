import { Injectable } from '@angular/core';
import {Apartment} from "./apartment";

const SUPPORTED_COUNTRIES: string[][] = [
    ["DE", "Deutschland"],
    ["NL", "Niederlande"],
    ["GB", "Großbritannien"]
];

@Injectable()
export class CountryService {

    static getSupportedCountries() {
        return SUPPORTED_COUNTRIES;
    }

    static getCountryNameByCode(countryCode) {
        return SUPPORTED_COUNTRIES.find(function (countryArray) {
            return countryArray[0] == countryCode;
        })[1];
    }
}